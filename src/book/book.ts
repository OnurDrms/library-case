import Sequelize, { DataTypes } from 'sequelize';
const db =  require('../database');
var sequelize = db.sequelize;
  var Book = db.sequelize.define('books', {
      'id': {
          type: DataTypes.INTEGER(),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
      },
      'name': {
          type: DataTypes.STRING(255)
      },
      'score': {
          type: DataTypes.FLOAT()
      }
  },
  {
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    sequelize,
  });

  module.exports = Book;