var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var basename = path.basename(__filename);

let DATABASE_URL = 'postgres://wgmqdqtjvrkkfi:50f3b74abcb2fd74b22bb2ba23b69cd75ab6cb80f094c70ab3944f5696ac6091@ec2-54-228-250-82.eu-west-1.compute.amazonaws.com:5432/ddfgrgtauhvm2c?ssl=true';

const sequelize = new Sequelize(DATABASE_URL);

var db = {
    Sequelize: Sequelize,
    sequelize: sequelize
};
fs.readdirSync(__dirname)
.filter((file: string) => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.ts');
})
.forEach((file: any) => {
    var model = db.sequelize['import'](path.join(__dirname, file));
    db = {...db,[model.name]: model};
});

module.exports = db;