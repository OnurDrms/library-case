import { Request, Response } from 'express';
const { validationResult } = require('express-validator');

const User  = require('../../user/user');
const Book  = require('../../book/book');

export const getUsers = async (req: Request, res: Response) => {
    try {
        const users = await User.findAll({});
        return res.status(200).json({ users });
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const getUserById = async (req: Request, res: Response)  => {
    const id = parseInt(req.params.id);
    try {
        const user = await User.findOne({
          where: { id: id }
        });
        if (user) {
          return res.status(200).json({ user });
        }
        return res.status(404).send("User with the specified ID does not exists");
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const createUser = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const {name} = req.body;
    try {
        const pg = require('pg');
        const pool = new pg.Pool({
        user: 'wgmqdqtjvrkkfi',
        host: 'ec2-54-228-250-82.eu-west-1.compute.amazonaws.com',
        database: 'ddfgrgtauhvm2c',
        password: '50f3b74abcb2fd74b22bb2ba23b69cd75ab6cb80f094c70ab3944f5696ac6091',
        port: '5432',
        ssl: true
        });
        pool.query("CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, name varchar(40),books JSON)", (err: any, res: any) => {
        //console.log(err, res);
        pool.end();
        });
        const user = await User.create({name: name});
        return res.status(201).json({user});
      } catch (error) {
        return res.status(500).json({ error: error.message });
      }
      
};

export const updateUser = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const id = parseInt(req.params.id);
    const { name,books } = req.body;

    try {
        const [updated] = await User.update({name:name,books:books}, {
          where: { id: id }
        });
        if (updated) {
          const updatedUser = await User.findOne({ where: { id: id } });
          return res.status(200).json({ user: updatedUser });
        }
        throw new Error("User not found");
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const deleteUser = async (req: Request, res: Response) => {
    const id = parseInt(req.params.id);

    try {
        const deleted = await User.destroy({
          where: { id: id }
        });
        if (deleted) {
          return res.status(204).send("User deleted");
        }
        throw new Error("User not found");
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const postBorrow = async (req: Request, res: Response) => {

  let present;let past;
  const userid = parseInt(req.params.id);
  const bookid = parseInt(req.params.bookid);
  

  try {
  const user = await User.findOne({
      where: { id: userid }
    });
    if(!user){
      return res.json({message: "Kullanıcı kayıtlarda yok."});
    }
  } catch (error) {
    return res.json({message: error});
  }
  
  const UserReturn = await User.findOne({ where: { id: userid } });
  const BookBorrow = await Book.findOne({ where: { id: bookid } });
  const users = await User.findAll({});
 
  for(let i=0;i<users.length;i++){
    if(users[i].books != null && users[i].books.present.length > 0){
      for(let k=0;k<users[i].books.present.length;k++){
        if(users[i].books.present[k].name == BookBorrow.name){
          return res.json({message: "Kitap başka bir kullanıcı üzerine kayıtlı."});
        }
      }
    }
  }

  
  if(UserReturn.books){
    present = UserReturn.books.present;
    past = UserReturn.books.past;
    const BookBorrow = await Book.findOne({ where: { id: bookid } });
    let presentObject = {name: BookBorrow.name};
    await User.update({books:{past: past,present:[...present,presentObject]}}, {
    where: { id: userid }
    });
    return res.status(200).json({message:"Book borrow"});
  }else{
    const BookBorrow = await Book.findOne({ where: { id: bookid } });
    let presentObject = {name: BookBorrow.name};
      if(!UserReturn.hasOwnProperty("books")){
        await User.update({books:{past:[],present:[presentObject]}}, {
        where: { id: userid }
        });
        return res.status(200).json({message:"Book borrow"});
      }else{
        past = UserReturn.books.past;
        await User.update({books:{past:past,present:[presentObject]}}, {
        where: { id: userid }
        });
        return res.status(200).json({message:"Book borrow"});
      }
  }    
};

export const postReturn = async (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let past;let newScore;let userScore;
  const userid = parseInt(req.params.id);
  const bookid = parseInt(req.params.bookid);
  const { score } = req.body;

  const BookReturn = await Book.findOne({ where: { id: bookid } });
  if(BookReturn.score){
    newScore = (parseFloat(score) + parseFloat(BookReturn.score)) / 2.0;
  }else{
    newScore = score;
  }
  await Book.update({score:newScore}, {
    where: { id: bookid }
  });
  const UserReturn = await User.findOne({ where: { id: userid } });
  if(UserReturn.books){
    past = UserReturn.books.past;
    let userScore = UserReturn.books.past.length+1;
    let pastObject = {name: BookReturn.name,userScore:userScore};
    if(UserReturn.books.present.length>1){
        let index = UserReturn.books.present.findIndex((x: { name: string; }) => x.name === BookReturn.name);
        UserReturn.books.present.splice(index, 1);
        await User.update({books:{past:[...past,pastObject],present:UserReturn.books.present}}, {
        where: { id: userid }
        });
        return res.status(200).json({message:"Book return"});
    }else{
        await User.update({books:{past:[...past,pastObject],present:[]}}, {
        where: { id: userid }
        });
        return res.status(200).json({message:"Book return"});
    }
  }else{
    if(UserReturn.books){
      userScore = UserReturn.books.past.length+1;
    }else{
      userScore = 1;
    }
    let pastObject = {name: BookReturn.name,userScore:userScore};
    await User.update({books:{past:[pastObject],present:[]}}, {
      where: { id: userid }
    });
    return res.status(200).json({message:"Book return"});
  }
};