import { Request, Response } from 'express';
const { validationResult } = require('express-validator');

const Book  = require('../../book/book');


export const getBooks = async (req: Request, res: Response) => {
    try {
        const books = await Book.findAll({});
        return res.status(200).json({ books });
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const getBookById = async (req: Request, res: Response)  => {
    const id = parseInt(req.params.id);
    try {
        const book = await Book.findOne({
          where: { id: id }
        });
        if (book) {
          return res.status(200).json({ book });
        }
        return res.status(404).send("Book with the specified ID does not exists");
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const createBook = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const {name,score} = req.body;
    try {
        const pg = require('pg');
        const pool = new pg.Pool({
        user: 'wgmqdqtjvrkkfi',
        host: 'ec2-54-228-250-82.eu-west-1.compute.amazonaws.com',
        database: 'ddfgrgtauhvm2c',
        password: '50f3b74abcb2fd74b22bb2ba23b69cd75ab6cb80f094c70ab3944f5696ac6091',
        port: '5432',
        ssl: true
        });
        pool.query("CREATE TABLE IF NOT EXISTS books(id SERIAL PRIMARY KEY, name varchar(40),score INTEGER)", (err: any, res: any) => {
        //console.log(err, res);
        pool.end();
        });
        const book = await Book.create({name: name,score:score});
        return res.status(201).json({book});
      } catch (error) {
        return res.status(500).json({ error: error.message });
      }
      
};

export const updateBook = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const id = parseInt(req.params.id);
    const { name } = req.body;

    try {
        const [updated] = await Book.update({name:name}, {
          where: { id: id }
        });
        if (updated) {
          const updatedBook = await Book.findOne({ where: { id: id } });
          return res.status(200).json({ book: updatedBook });
        }
        throw new Error("Book not found");
      } catch (error) {
        return res.status(500).send(error.message);
      }
};

export const deleteBook = async (req: Request, res: Response) => {
    const id = parseInt(req.params.id);

    try {
        const deleted = await Book.destroy({
          where: { id: id }
        });
        if (deleted) {
          return res.status(204).send("Book deleted");
        }
        throw new Error("Book not found");
      } catch (error) {
        return res.status(500).send(error.message);
      }
};