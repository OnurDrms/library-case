import {Router} from 'express';
const { body } = require('express-validator');
const router = Router();

import { getUsers, getUserById, createUser, updateUser, deleteUser ,postBorrow, postReturn} from '../controllers/user/index.controller';
import { getBooks, getBookById, createBook, updateBook, deleteBook } from '../controllers/book/index.controller';

router.get('/users', getUsers);
router.get('/users/:id', getUserById);
router.post('/users', [body('name').isLength({max:50})],createUser);
router.put('/users/:id',[body('name').isLength({max:50})],updateUser)
router.delete('/users/:id', deleteUser);

router.get('/books', getBooks);
router.get('/books/:id', getBookById);
router.post('/books', [body('name').isLength({max:100})],createBook);
router.put('/books/:id', [body('name').isLength({max:100})],updateBook)
router.delete('/books/:id', deleteBook);

router.post('/users/:id/borrow/:bookid',postBorrow);
router.post('/users/:id/return/:bookid',[body('score').isNumeric()],postReturn);

export default router;