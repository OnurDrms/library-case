import Sequelize, { DataTypes } from 'sequelize';
const db =  require('../database');
var sequelize = db.sequelize;
  var User = db.sequelize.define('users', {
      'id': {
          type: DataTypes.INTEGER(),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
      },
      'name': {
          type: DataTypes.STRING(255)
      },
      'books': {
        type: DataTypes.JSON,
        'past': {
            type: DataTypes.ARRAY,
            'name': {
                type: DataTypes.STRING(255)
            },
            'userScore': {
                type: DataTypes.INTEGER()
            },
        },
        'present': {
            type: DataTypes.ARRAY,
            'name': {
                type: DataTypes.STRING(255)
            },
        },
      }
  },
  {
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    sequelize,
  });

  module.exports = User;