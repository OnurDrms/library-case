"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db = require('../database');
var sequelize = db.sequelize;
var User = db.sequelize.define('users', {
    'id': {
        type: sequelize_1.DataTypes.INTEGER(),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    'name': {
        type: sequelize_1.DataTypes.STRING(255)
    },
    'books': {
        type: sequelize_1.DataTypes.JSON,
        'past': {
            type: sequelize_1.DataTypes.ARRAY,
            'name': {
                type: sequelize_1.DataTypes.STRING(255)
            },
            'userScore': {
                type: sequelize_1.DataTypes.INTEGER()
            },
        },
        'present': {
            type: sequelize_1.DataTypes.ARRAY,
            'name': {
                type: sequelize_1.DataTypes.STRING(255)
            },
        },
    }
}, {
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    sequelize,
});
module.exports = User;
