"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { validationResult } = require('express-validator');
const User = require('../../user/user');
const Book = require('../../book/book');
exports.getUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const users = yield User.findAll({});
        return res.status(200).json({ users });
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.getUserById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = parseInt(req.params.id);
    try {
        const user = yield User.findOne({
            where: { id: id }
        });
        if (user) {
            return res.status(200).json({ user });
        }
        return res.status(404).send("User with the specified ID does not exists");
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.createUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    const { name } = req.body;
    try {
        const pg = require('pg');
        const pool = new pg.Pool({
            user: 'wgmqdqtjvrkkfi',
            host: 'ec2-54-228-250-82.eu-west-1.compute.amazonaws.com',
            database: 'ddfgrgtauhvm2c',
            password: '50f3b74abcb2fd74b22bb2ba23b69cd75ab6cb80f094c70ab3944f5696ac6091',
            port: '5432',
            ssl: true
        });
        pool.query("CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, name varchar(40),books JSON)", (err, res) => {
            //console.log(err, res);
            pool.end();
        });
        const user = yield User.create({ name: name });
        return res.status(201).json({ user });
    }
    catch (error) {
        return res.status(500).json({ error: error.message });
    }
});
exports.updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    const id = parseInt(req.params.id);
    const { name, books } = req.body;
    try {
        const [updated] = yield User.update({ name: name, books: books }, {
            where: { id: id }
        });
        if (updated) {
            const updatedUser = yield User.findOne({ where: { id: id } });
            return res.status(200).json({ user: updatedUser });
        }
        throw new Error("User not found");
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = parseInt(req.params.id);
    try {
        const deleted = yield User.destroy({
            where: { id: id }
        });
        if (deleted) {
            return res.status(204).send("User deleted");
        }
        throw new Error("User not found");
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.postBorrow = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let present;
    let past;
    const userid = parseInt(req.params.id);
    const bookid = parseInt(req.params.bookid);
    try {
        const user = yield User.findOne({
            where: { id: userid }
        });
        if (!user) {
            return res.json({ message: "Kullanıcı kayıtlarda yok." });
        }
    }
    catch (error) {
        return res.json({ message: error });
    }
    const UserReturn = yield User.findOne({ where: { id: userid } });
    const BookBorrow = yield Book.findOne({ where: { id: bookid } });
    const users = yield User.findAll({});
    for (let i = 0; i < users.length; i++) {
        if (users[i].books != null && users[i].books.present.length > 0) {
            for (let k = 0; k < users[i].books.present.length; k++) {
                if (users[i].books.present[k].name == BookBorrow.name) {
                    return res.json({ message: "Kitap başka bir kullanıcı üzerine kayıtlı." });
                }
            }
        }
    }
    if (UserReturn.books) {
        present = UserReturn.books.present;
        past = UserReturn.books.past;
        const BookBorrow = yield Book.findOne({ where: { id: bookid } });
        let presentObject = { name: BookBorrow.name };
        yield User.update({ books: { past: past, present: [...present, presentObject] } }, {
            where: { id: userid }
        });
        return res.status(200).json({ message: "Book borrow" });
    }
    else {
        const BookBorrow = yield Book.findOne({ where: { id: bookid } });
        let presentObject = { name: BookBorrow.name };
        if (!UserReturn.hasOwnProperty("books")) {
            yield User.update({ books: { past: [], present: [presentObject] } }, {
                where: { id: userid }
            });
            return res.status(200).json({ message: "Book borrow" });
        }
        else {
            past = UserReturn.books.past;
            yield User.update({ books: { past: past, present: [presentObject] } }, {
                where: { id: userid }
            });
            return res.status(200).json({ message: "Book borrow" });
        }
    }
});
exports.postReturn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    let past;
    let newScore;
    let userScore;
    const userid = parseInt(req.params.id);
    const bookid = parseInt(req.params.bookid);
    const { score } = req.body;
    const BookReturn = yield Book.findOne({ where: { id: bookid } });
    if (BookReturn.score) {
        newScore = (parseFloat(score) + parseFloat(BookReturn.score)) / 2.0;
    }
    else {
        newScore = score;
    }
    yield Book.update({ score: newScore }, {
        where: { id: bookid }
    });
    const UserReturn = yield User.findOne({ where: { id: userid } });
    if (UserReturn.books) {
        past = UserReturn.books.past;
        let userScore = UserReturn.books.past.length + 1;
        let pastObject = { name: BookReturn.name, userScore: userScore };
        if (UserReturn.books.present.length > 1) {
            let index = UserReturn.books.present.findIndex((x) => x.name === BookReturn.name);
            UserReturn.books.present.splice(index, 1);
            yield User.update({ books: { past: [...past, pastObject], present: UserReturn.books.present } }, {
                where: { id: userid }
            });
            return res.status(200).json({ message: "Book return" });
        }
        else {
            yield User.update({ books: { past: [...past, pastObject], present: [] } }, {
                where: { id: userid }
            });
            return res.status(200).json({ message: "Book return" });
        }
    }
    else {
        if (UserReturn.books) {
            userScore = UserReturn.books.past.length + 1;
        }
        else {
            userScore = 1;
        }
        let pastObject = { name: BookReturn.name, userScore: userScore };
        yield User.update({ books: { past: [pastObject], present: [] } }, {
            where: { id: userid }
        });
        return res.status(200).json({ message: "Book return" });
    }
});
