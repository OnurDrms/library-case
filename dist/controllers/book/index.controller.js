"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { validationResult } = require('express-validator');
const Book = require('../../book/book');
exports.getBooks = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const books = yield Book.findAll({});
        return res.status(200).json({ books });
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.getBookById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = parseInt(req.params.id);
    try {
        const book = yield Book.findOne({
            where: { id: id }
        });
        if (book) {
            return res.status(200).json({ book });
        }
        return res.status(404).send("Book with the specified ID does not exists");
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.createBook = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    const { name, score } = req.body;
    try {
        const pg = require('pg');
        const pool = new pg.Pool({
            user: 'wgmqdqtjvrkkfi',
            host: 'ec2-54-228-250-82.eu-west-1.compute.amazonaws.com',
            database: 'ddfgrgtauhvm2c',
            password: '50f3b74abcb2fd74b22bb2ba23b69cd75ab6cb80f094c70ab3944f5696ac6091',
            port: '5432',
            ssl: true
        });
        pool.query("CREATE TABLE IF NOT EXISTS books(id SERIAL PRIMARY KEY, name varchar(40),score INTEGER)", (err, res) => {
            //console.log(err, res);
            pool.end();
        });
        const book = yield Book.create({ name: name, score: score });
        return res.status(201).json({ book });
    }
    catch (error) {
        return res.status(500).json({ error: error.message });
    }
});
exports.updateBook = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    const id = parseInt(req.params.id);
    const { name } = req.body;
    try {
        const [updated] = yield Book.update({ name: name }, {
            where: { id: id }
        });
        if (updated) {
            const updatedBook = yield Book.findOne({ where: { id: id } });
            return res.status(200).json({ book: updatedBook });
        }
        throw new Error("Book not found");
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
exports.deleteBook = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = parseInt(req.params.id);
    try {
        const deleted = yield Book.destroy({
            where: { id: id }
        });
        if (deleted) {
            return res.status(204).send("Book deleted");
        }
        throw new Error("Book not found");
    }
    catch (error) {
        return res.status(500).send(error.message);
    }
});
