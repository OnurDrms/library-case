"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db = require('../database');
var sequelize = db.sequelize;
var Book = db.sequelize.define('books', {
    'id': {
        type: sequelize_1.DataTypes.INTEGER(),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    'name': {
        type: sequelize_1.DataTypes.STRING(255)
    },
    'score': {
        type: sequelize_1.DataTypes.FLOAT()
    }
}, {
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    sequelize,
});
module.exports = Book;
